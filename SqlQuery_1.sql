﻿use mkr2

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Kiosk')
BEGIN
DROP TABLE Kiosk;

END;

CREATE TABLE Kiosk
(
    Kod INT IDENTITY PRIMARY KEY,
    Gaseta VARCHAR(100) NOT NULL,
    Jurnal VARCHAR(100) NOT NULL,
    QuantityOfPrimirnik INT, 
    price DECIMAL(18, 2),
   
);

INSERT INTO Kiosk (Gaseta, Jurnal, QuantityOfPrimirnik, price)
values
    ('Sonyachnick', 'Jurnal 1', 2, 4520.00),
    ('gaseta1', 'Jurnal 2',  5, 6000.00),
    ('gaseta2', 'Jurnal 3',  4, 2500.00),
    ('gaseta3', 'Jurnal 3',  1, 15000.00);




select * from Kiosk;


SELECT SUM(QuantityOfPrimirnik * price) AS TotalCostOfNewspapers
FROM Kiosk
WHERE Gaseta IS NOT NULL;


DECLARE @MinPrice DECIMAL(18, 2) = 2000.00;  
DECLARE @MaxPrice DECIMAL(18, 2) = 5000.00; 

SELECT COUNT(*) AS NumberOfJournalsInRange
FROM Kiosk
WHERE Jurnal IS NOT NULL AND price BETWEEN @MinPrice AND @MaxPrice;