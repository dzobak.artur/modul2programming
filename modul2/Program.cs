﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace modul2
{

    internal class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=mkr2;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM Kiosk", connection))
                    {
                        Console.WriteLine("Дані з таблиці Kiosk:");
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine($"Gaseta: {reader["Gaseta"]}, Jurnal: {reader["Jurnal"]}, QuantityOfPrimirnik: {reader["QuantityOfPrimirnik"]}, Price: {reader["price"]}");
                            }
                        }
                    }

                    using (SqlCommand command = new SqlCommand("SELECT SUM(QuantityOfPrimirnik * price) AS TotalCostOfNewspapers FROM Kiosk WHERE Gaseta IS NOT NULL", connection))
                    {
                        object totalCost = command.ExecuteScalar();
                        Console.WriteLine($"\nЗагальна вартість усіх газет: {totalCost} грн");
                    }

                    decimal minPrice = 2000.00m;
                    decimal maxPrice = 5000.00m;

                    using (SqlCommand command = new SqlCommand("SELECT COUNT(*) AS NumberOfJournalsInRange FROM Kiosk WHERE Jurnal IS NOT NULL AND price BETWEEN @MinPrice AND @MaxPrice", connection))
                    {
                        command.Parameters.AddWithValue("@MinPrice", minPrice);
                        command.Parameters.AddWithValue("@MaxPrice", maxPrice);

                        object numberOfJournals = command.ExecuteScalar();
                        Console.WriteLine($"\nКількість журналів у визначеному діапазоні цін: {numberOfJournals}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Помилка: {ex.Message}");
            }

            Console.ReadLine();
        }
    }
}
